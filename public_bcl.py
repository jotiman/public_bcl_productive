import os
import pandas as pd
from tabulate import tabulate
from tkinter import filedialog
from tkinter import *
import shutil
import numpy as np

# To get rid of extra lines of code at the print statement, a simple lambda function can be written in this way:
df_tabulate = lambda df: tabulate(df, headers='keys',tablefmt='psql')

# Select columns
def select_columns(df, checkbox_vars):
    # print(df.columns)    # for testing
    # df = df[0:20]       # for testing
    all_column_names = df.columns.tolist()
    all_column_names = np.array(all_column_names)
    selected_column_names = all_column_names[checkbox_vars]
    return df[selected_column_names]


# Get all csv file names and paths from a folder
def get_csv_list_from(folder = r"." ):
    csv_list = []
    for root, dirs, files in os.walk(folder, topdown=True):
       for name in files:
            if ".csv" in name:
                print(f"{name}  loaded")
                csv_list.append((name,os.path.join(root, name)))
       break
    return csv_list


# Show all csv columns as checkboxes in a window
def show_csv_columns(path):
    checkboxes = []
    checkbox_vars = []
    df = pd.read_csv(path[1], sep=r"\t",engine='python')
    column_names = df.columns.tolist()
    root.deiconify()

    def callBackFunc():
        # print([var.get() for var in checkbox_vars])     # for testing
        pass

    default_cols = ['ZS AppIID',
                    'Parent ID',
                    'Parent Version',
                    'Zuständige Stelle',
                    'Owner E-Mail',
                    'Verfahren',
                    'Start-URL',
                    'Antragsmanagement Version',
                    'Status']

    for row, column in enumerate(column_names, 1):
        checkbox_vars.append(BooleanVar())
        box = Checkbutton(root, text=column, variable = checkbox_vars[-1], command = callBackFunc)
        if column in default_cols:
            checkbox_vars[-1].set(True)
            box.config(state = DISABLED)
        box.grid(row=row, column=0, sticky=W)
        checkboxes.append(box)
    exit_button = Button(root, text="Create CSV", command=root.destroy)
    exit_button.grid(row=row+2, column=0)
    root.mainloop()
    return [check.get() for check in checkbox_vars]


# Load csv files into a dataframe
def load_csv_into_df(folder):
    df_list = []
    csv_list = get_csv_list_from(folder)
    checkbox_vars = show_csv_columns(csv_list[0])
    for _,path in csv_list:
        csv_content = pd.read_csv(path, sep=r"\t",engine='python')
        df_list.append(csv_content)

    return pd.concat(df_list, ignore_index=True), checkbox_vars


def filter_dataframe(df_merged,bcl= False):
    if bcl == True:
        df_merged = df_merged[df_merged['Antragsmanagement Version'] == 3]  # only BCLs
    df_merged = df_merged[~df_merged['Owner E-Mail'].str.contains("fjd.de", na=False, case=False)]  # no fjd domains
    df_merged = df_merged[~df_merged['Owner E-Mail'].str.contains("it.niedersachsen.de", na=False, case=False)]  # no it.niedersachsen domains
    df_merged = df_merged[~df_merged['Zuständige Stelle'].str.contains("Test", na=False, case=False)] # no "Test" in 'Zuständige Stelle'
    df_merged = df_merged[~df_merged['Verfahren'].str.contains("Test", na=False, case=False)] # no "Test" in 'Verfahren'
    df_merged = df_merged[df_merged['Status']=="aktiv"] # only aktiv in Status
    return df_merged


def add_columns_to(df_merged):
    # new column -> owner email domain
    df_merged['Admin'] = df_merged['Owner E-Mail'].str.split(pat="@")
    df_merged['Admin'] = df_merged['Admin'].str[-1]
    df_merged['Admin'] = df_merged['Admin'].str.rsplit(pat=".", n=1)
    df_merged['Admin'] = df_merged['Admin'].str[0]

    # new column ->  System domain
    df_merged['System'] = df_merged['Start-URL'].str.split(pat="https://")
    df_merged['System'] = df_merged['System'].str[1]
    df_merged['System'] = df_merged['System'].str.split(pat="\.de")
    df_merged['System'] = df_merged['System'].str[0]

    return df_merged


# Show dialog to select folder with csv files
def select_csv_folder():
    folder_selected = filedialog.askdirectory()
    return folder_selected


# Save merged data frame into subfolder "output"
def save_df_to(folder_selected, df_merged, file_name, delete_output_folder ):
    if os.path.exists(fr"{folder_selected}/output/"):
        if delete_output_folder:
            shutil.rmtree(fr"{folder_selected}/output/")
            os.mkdir(fr"{folder_selected}/output/")
    else:
        os.mkdir(fr"{folder_selected}/output/")
    df_merged.to_csv(fr"{folder_selected}/output/{file_name}.csv", sep='\t')
    print(fr"{folder_selected}/output/{file_name}.csv   saved")

# Merge and create a csv file from different GOVOS Systems
def create_report(df_merged , bcl, selected_folder, file_name, delete_output_folder):
    df_merged_all = filter_dataframe(df_merged, bcl)
    df_merged_all = select_columns(df_merged_all, checkbox_vars)
    df_merged_all = add_columns_to(df_merged_all)
    save_df_to(selected_folder, df_merged_all, file_name, delete_output_folder)

    # print(df_tabulate(df_merged_all[:]))
    # print(df_merged_all.info())

if __name__ == '__main__':
    # Create main window
    root = Tk()
    root.title("Select Columns ")
    root.geometry("200x800")
    root.withdraw()
    # tp1 = Toplevel(root)  # to have more windows this is the way
    # tp1.withdraw()

    selected_folder = select_csv_folder()

    if selected_folder != '':
        df_merged, checkbox_vars = load_csv_into_df(selected_folder)

        create_report(df_merged, False, selected_folder, "Aktive_Verfahren", True)
        create_report(df_merged, True, selected_folder, "Aktive_BCL_Verfahren", False)

    else:
        print("No folder selected !")
# todo: nan checking in columns -> parent Ids
# todo: handle close button different from create button